import EmptyLogger from './EmptyLogger'
import Logger from './Logger';

const checkForLogger = () => {
  try {
    return require('@s2m/logger');
  } catch (e) {
    return null;
  }
};

export type LoggerFactory = (name: string) => Logger;

const logger = checkForLogger();
const getLogger: LoggerFactory = logger ? logger.getLogger : () => new EmptyLogger();

export {
  getLogger,
}
