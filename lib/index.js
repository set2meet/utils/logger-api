"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLogger = void 0;
const EmptyLogger_1 = __importDefault(require("./EmptyLogger"));
const checkForLogger = () => {
    try {
        return require('@s2m/logger');
    }
    catch (e) {
        return null;
    }
};
const logger = checkForLogger();
const getLogger = logger ? logger.getLogger : () => new EmptyLogger_1.default();
exports.getLogger = getLogger;
