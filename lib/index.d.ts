import Logger from './Logger';
export declare type LoggerFactory = (name: string) => Logger;
declare const getLogger: LoggerFactory;
export { getLogger, };
