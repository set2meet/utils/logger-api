"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EmptyLogger {
    trace(message) {
        //
    }
    debug(message) {
        //
    }
    info(message) {
        //
    }
    warn(message) {
        //
    }
    error(message) {
        //
    }
    fatal(message) {
        //
    }
}
exports.default = EmptyLogger;
