interface Logger {
    trace(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    trace(message: string): void;
    debug(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    debug(message: string): void;
    info(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    info(message: string): void;
    warn(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    warn(message: string): void;
    error(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    error(message: string): void;
    fatal(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    fatal(message: string): void;
}
export default Logger;
