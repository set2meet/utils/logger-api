import Logger from './Logger';
export default class EmptyLogger implements Logger {
    trace(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    debug(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    info(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    warn(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    error(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
    fatal(obj: Error | {
        [key: string]: any;
    } | string | any, ...params: any[]): void;
}
